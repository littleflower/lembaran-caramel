<?php

/* /home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-garapan/content.htm */
class __TwigTemplate_64eb1336ceda66acc5c2d7b413336a2b3525469b69895d818228327734baf260 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["posts"] = $this->getAttribute(($context["blogPosts"] ?? null), "posts", array());
        // line 2
        echo "
<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"card z-depth-2 collection with-header\">
            <div class=\"collection-header\">
                <p/>
                <h2 class=\"lt-title\">Garapan</h2>
                <p class=\"flow-text\">Garapan spesial buat kamu yang spesial ❤</p>
            </div>
            <div class=\"collection-item\" style=\"overflow: hidden;\">
                ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 13
            echo "                <div class=\"lt-col col m4 s12\">
                    <div class=\"card large z-depth-1\">
                        <div class=\"card-image\">
                            <a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\">
                            ";
            // line 17
            if ($this->getAttribute($this->getAttribute($context["post"], "featured_images", array()), "count", array())) {
                // line 18
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute($context["post"], "featured_images", array()), 1, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 19
                    echo "                            <img class=\"responsive-img activator\" data-src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename", array()), "html", null, true);
                    echo "\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "path", array()), "html", null, true);
                    echo "\"
                                 alt=\"";
                    // line 20
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "description", array()) != null)) ? ($this->getAttribute($context["image"], "description", array())) : ($this->getAttribute($context["image"], "filename", array()))), "html", null, true);
                    echo "\"
                                 title=\"";
                    // line 21
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "title", array()) != null)) ? ($this->getAttribute($context["image"], "title", array())) : ($this->getAttribute($context["post"], "title", array()))), "html", null, true);
                    echo "\">
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "                            ";
            } else {
                // line 24
                echo "                            <img class=\"responsive-img activator\" alt=\"image\" src=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/pages/1701/header-404.jpg");
                echo "\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
                echo "\">
                            ";
            }
            // line 26
            echo "                            </a>
                        </div>
                        <div class=\"card-content\">
                             <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\"><span class=\"card-title\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</span></a><br/>
                            ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "summary", array()), "html", null, true);
            echo "
                        </div>
                        <div class=\"card-action\">
                            ";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["post"], "categories", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 34
                echo "                                ";
                if (($this->getAttribute($context["category"], "name", array()) == "Garapan Selesai")) {
                    // line 35
                    echo "                                    <span class=\"chip green left\">Selesai</span>
                                ";
                } elseif (($this->getAttribute(                // line 36
$context["category"], "name", array()) == "Garapan Lancar")) {
                    // line 37
                    echo "                                    <span class=\"chip blue left\">Lancar</span>
                                ";
                } elseif (($this->getAttribute(                // line 38
$context["category"], "name", array()) == "Garapan Terhambat")) {
                    // line 39
                    echo "                                    <span class=\"chip yellow darken-3 left\">Lancar?</span>
                                ";
                } elseif (($this->getAttribute(                // line 40
$context["category"], "name", array()) == "Garapan Batal")) {
                    // line 41
                    echo "                                    <span class=\"chip red left\">Batal</span>
                                ";
                } elseif (($this->getAttribute(                // line 42
$context["category"], "name", array()) == "Sedang Tayang")) {
                    // line 43
                    echo "                                    <span class=\"chip purple left\">Tayang</span>
                                ";
                } elseif (call_user_func_array($this->env->getFunction('str_contains')->getCallable(), array("contains", $this->getAttribute(                // line 44
$context["category"], "slug", array()), "join-"))) {
                    // line 45
                    echo "                                    <span class=\"chip pink left\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                    echo "</span>
                                ";
                }
                // line 47
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "                        </div>
                    </div>
                </div>
            
                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 53
            echo "                <h5><center class=\"align-center center\">";
            echo twig_escape_filter($this->env, ($context["noPostsMessage"] ?? null), "html", null, true);
            echo "</center></h5>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "            </div>
            <div class=\"collection-item\">
                <span class=\"center-align\">
                    ";
        // line 58
        if (($this->getAttribute(($context["posts"] ?? null), "lastPage", array()) > 1)) {
            // line 59
            echo "                        ";
            if (($this->getAttribute(($context["posts"] ?? null), "currentPage", array()) > 1)) {
                // line 60
                echo "                            <a href=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => ($this->getAttribute(($context["posts"] ?? null), "currentPage", array()) - 1)));
                echo "\" class=\"btn-floating btn-large waves-effect waves-light left pink\"><i class=\"material-icons\">chevron_left</i></a>
                        ";
            }
            // line 62
            echo "                        ";
            if (($this->getAttribute(($context["posts"] ?? null), "lastPage", array()) > $this->getAttribute(($context["posts"] ?? null), "currentPage", array()))) {
                // line 63
                echo "                            <a href=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => ($this->getAttribute(($context["posts"] ?? null), "currentPage", array()) + 1)));
                echo "\" class=\"btn-floating btn-large waves-effect waves-light right pink\"><i class=\"material-icons\">chevron_right</i></a>
                        ";
            }
            // line 65
            echo "                    <ul class=\"pagination\">
                        ";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute(($context["posts"] ?? null), "lastPage", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 67
                echo "                        <li class=\"";
                echo ((($this->getAttribute(($context["posts"] ?? null), "currentPage", array()) == $context["page"])) ? ("active pink") : (null));
                echo "\">
                            <a href=\"";
                // line 68
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => $context["page"]));
                echo "\" class=\"waves-effect\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
                        </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "                        
                    </ul>
                    ";
        }
        // line 73
        echo "                </span>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-garapan/content.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 73,  211 => 70,  200 => 68,  195 => 67,  191 => 66,  188 => 65,  182 => 63,  179 => 62,  173 => 60,  170 => 59,  168 => 58,  163 => 55,  154 => 53,  145 => 48,  139 => 47,  133 => 45,  131 => 44,  128 => 43,  126 => 42,  123 => 41,  121 => 40,  118 => 39,  116 => 38,  113 => 37,  111 => 36,  108 => 35,  105 => 34,  101 => 33,  95 => 30,  89 => 29,  84 => 26,  76 => 24,  73 => 23,  65 => 21,  61 => 20,  54 => 19,  49 => 18,  47 => 17,  43 => 16,  38 => 13,  33 => 12,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set posts = blogPosts.posts %}

<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"card z-depth-2 collection with-header\">
            <div class=\"collection-header\">
                <p/>
                <h2 class=\"lt-title\">Garapan</h2>
                <p class=\"flow-text\">Garapan spesial buat kamu yang spesial ❤</p>
            </div>
            <div class=\"collection-item\" style=\"overflow: hidden;\">
                {% for post in posts %}
                <div class=\"lt-col col m4 s12\">
                    <div class=\"card large z-depth-1\">
                        <div class=\"card-image\">
                            <a href=\"{{ post.url }}\">
                            {% if post.featured_images.count %}
                            {% for image in post.featured_images|slice(1, 1) %}
                            <img class=\"responsive-img activator\" data-src=\"{{ image.filename }}\" src=\"{{ image.path }}\"
                                 alt=\"{{ image.description != null ? image.description : image.filename }}\"
                                 title=\"{{ image.title != null ? image.title : post.title }}\">
                            {% endfor %}
                            {% else %}
                            <img class=\"responsive-img activator\" alt=\"image\" src=\"{{ 'assets/img/pages/1701/header-404.jpg'|theme }}\" href=\"{{ post.url }}\">
                            {% endif %}
                            </a>
                        </div>
                        <div class=\"card-content\">
                             <a href=\"{{ post.url }}\"><span class=\"card-title\">{{ post.title }}</span></a><br/>
                            {{ post.summary }}
                        </div>
                        <div class=\"card-action\">
                            {% for category in post.categories %}
                                {% if category.name == \"Garapan Selesai\" %}
                                    <span class=\"chip green left\">Selesai</span>
                                {% elseif category.name == \"Garapan Lancar\" %}
                                    <span class=\"chip blue left\">Lancar</span>
                                {% elseif category.name == \"Garapan Terhambat\" %}
                                    <span class=\"chip yellow darken-3 left\">Lancar?</span>
                                {% elseif category.name == \"Garapan Batal\" %}
                                    <span class=\"chip red left\">Batal</span>
                                {% elseif category.name == \"Sedang Tayang\" %}
                                    <span class=\"chip purple left\">Tayang</span>
                                {% elseif str_contains(category.slug, 'join-') %}
                                    <span class=\"chip pink left\">{{ category.name }}</span>
                                {% endif %}
                            {% endfor %}
                        </div>
                    </div>
                </div>
            
                {% else %}
                <h5><center class=\"align-center center\">{{ noPostsMessage }}</center></h5>
                {% endfor %}
            </div>
            <div class=\"collection-item\">
                <span class=\"center-align\">
                    {% if posts.lastPage > 1 %}
                        {% if posts.currentPage > 1 %}
                            <a href=\"{{ this.page.baseFileName|page({ (pageParam): (posts.currentPage-1) }) }}\" class=\"btn-floating btn-large waves-effect waves-light left pink\"><i class=\"material-icons\">chevron_left</i></a>
                        {% endif %}
                        {% if posts.lastPage > posts.currentPage %}
                            <a href=\"{{ this.page.baseFileName|page({ (pageParam): (posts.currentPage+1) }) }}\" class=\"btn-floating btn-large waves-effect waves-light right pink\"><i class=\"material-icons\">chevron_right</i></a>
                        {% endif %}
                    <ul class=\"pagination\">
                        {% for page in 1..posts.lastPage %}
                        <li class=\"{{ posts.currentPage == page ? 'active pink' : null }}\">
                            <a href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}\" class=\"waves-effect\">{{ page }}</a>
                        </li>
                        {% endfor %}                        
                    </ul>
                    {% endif %}
                </span>
            </div>
        </div>
    </div>
</div>", "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-garapan/content.htm", "");
    }
}
