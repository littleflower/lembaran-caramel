<?php

/* /home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/daftar.htm */
class __TwigTemplate_281866f3b05bcd8a9b848ef6715b128402fffed6325f44822f1ee25373ed0a8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <div class=\"card z-depth-2 collection with-header\">
                <div class=\"collection-header\">
                    <p/>
                    <h2 class=\"lt-title\">Daftar</h2>
                    <p class=\"flow-text\">Yuk segera mendaftar! ^^</p>
                </div>
                <div class=\"card-image\">
                    <img src=\"http://caramel.id/storage/app/media/13613234_1047194122043271_3254895436738790790_o.jpg\"/>
                </div>
                <div class=\"card-content\">
                    <p>Kamu mau mendaftar menjadi staff? Duh, manisnya! Segera kirim pesan melalui fanspage kami. Kami sangat menantikanmu! ❤</p><br>
                    <p>Oh iya, pendaftaran staff selalu dibuka. Jangan sungkan mendaftar kapanpun kamu siap.</p><br>
                    <p>Syaratnya bisa kamu baca sendiri nih:</p>
                    <ol>
                        <li><b>Translator</b> : Penerjemah Anime, boleh terjemah anime lama maupun baru, terserah kalian (no Ecchi desu >///<), minim bisa bahasa inggris, nilai tambah buat yg bisa bahasa jepang.</li>
                        <br>
                        <li><b>Typesetter</b> : Penata rias/pemberi tanda, tidak perlu berpengalaman, bisa sambil belajar</li>
                        <br>
                        <li><b>KaraFx</b> : Pembuat efek karaoke, minim hafal ASSTags, dan bisa buat efek simpel, entar dilatih lagi</li>
                        <br>
                        <li><b>Encode</b> : Mengecilkan video, dan menggabungkannya dengan subtitlenya, minim bisa MeGUI, super sekali kalau bisa CLI ^_^</li>
                        <br>
                        <li><b>Web Maintainer</b> : HTML + CSS, mengerti PHP + Laravel diutamakan. :p</li>
                        <br>
                        <li><b>Semua</b> : staff kami nyaman dengan keberadaanmu dan tergantung keberuntungan kamu! ~ ehehe..</p>
                    </ol>
                    <br>
                    <p>Cari formulirnya, ya? Langsung saja wawancara dengan para staff lewat fanspage / discord.</p>
                </div>
                <div class=\"card-action\">
                    <a href=\"https://www.facebook.com/caramelfansub/\" target=\"_blank\" class=\"btn-floating btn-large waves-effect waves-light blue darken-4\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a>
                    <a href=\"https://discord.gg/7zQeP3\" target=\"_blank\" class=\"btn-floating btn-large waves-effect waves-light deep-purple\"><i class=\"fa fa-gamepad\" aria-hidden=\"true\"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>



<div class=\"lt-vertical-spacer\"></div>

<div class=\"card z-depth-2\">

    
</div>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/daftar.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <div class=\"card z-depth-2 collection with-header\">
                <div class=\"collection-header\">
                    <p/>
                    <h2 class=\"lt-title\">Daftar</h2>
                    <p class=\"flow-text\">Yuk segera mendaftar! ^^</p>
                </div>
                <div class=\"card-image\">
                    <img src=\"http://caramel.id/storage/app/media/13613234_1047194122043271_3254895436738790790_o.jpg\"/>
                </div>
                <div class=\"card-content\">
                    <p>Kamu mau mendaftar menjadi staff? Duh, manisnya! Segera kirim pesan melalui fanspage kami. Kami sangat menantikanmu! ❤</p><br>
                    <p>Oh iya, pendaftaran staff selalu dibuka. Jangan sungkan mendaftar kapanpun kamu siap.</p><br>
                    <p>Syaratnya bisa kamu baca sendiri nih:</p>
                    <ol>
                        <li><b>Translator</b> : Penerjemah Anime, boleh terjemah anime lama maupun baru, terserah kalian (no Ecchi desu >///<), minim bisa bahasa inggris, nilai tambah buat yg bisa bahasa jepang.</li>
                        <br>
                        <li><b>Typesetter</b> : Penata rias/pemberi tanda, tidak perlu berpengalaman, bisa sambil belajar</li>
                        <br>
                        <li><b>KaraFx</b> : Pembuat efek karaoke, minim hafal ASSTags, dan bisa buat efek simpel, entar dilatih lagi</li>
                        <br>
                        <li><b>Encode</b> : Mengecilkan video, dan menggabungkannya dengan subtitlenya, minim bisa MeGUI, super sekali kalau bisa CLI ^_^</li>
                        <br>
                        <li><b>Web Maintainer</b> : HTML + CSS, mengerti PHP + Laravel diutamakan. :p</li>
                        <br>
                        <li><b>Semua</b> : staff kami nyaman dengan keberadaanmu dan tergantung keberuntungan kamu! ~ ehehe..</p>
                    </ol>
                    <br>
                    <p>Cari formulirnya, ya? Langsung saja wawancara dengan para staff lewat fanspage / discord.</p>
                </div>
                <div class=\"card-action\">
                    <a href=\"https://www.facebook.com/caramelfansub/\" target=\"_blank\" class=\"btn-floating btn-large waves-effect waves-light blue darken-4\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a>
                    <a href=\"https://discord.gg/7zQeP3\" target=\"_blank\" class=\"btn-floating btn-large waves-effect waves-light deep-purple\"><i class=\"fa fa-gamepad\" aria-hidden=\"true\"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>



<div class=\"lt-vertical-spacer\"></div>

<div class=\"card z-depth-2\">

    
</div>", "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/daftar.htm", "");
    }
}
