<?php

/* /home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-home/navigation.htm */
class __TwigTemplate_15535f7414fb4da1d433189720c9db2575ae337f61f10c89d2c3ad576e05ba5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"lt-navbar navbar-fixed\">
  <nav class=\"lt-no-background\">
    <div class=\"nav-wrapper\">
      <div class=\"lt-container container\">
        <a href=\"#bittersweet\" class=\"brand-logo white-text\">Caramel</a>
        <a href=\"#\" data-activates=\"mobile-menu\" class=\"lt-mobile-toggle button-collapse\"><i class=\"material-icons\">menu</i></a>
        <ul class=\"right hide-on-med-and-down\">
          <li><a href=\"#rilisan\">Rilisan</a></li>
          <li><a href=\"";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("garapan");
        echo "\">Garapan</a></li>
          <li><a href=\"";
        // line 10
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("tentang");
        echo "\">Tentang</a></li>
          <li><a href=\"";
        // line 11
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("daftar");
        echo "\">Daftar</a></li>
        </ul>
        <ul class=\"side-nav\" id=\"mobile-menu\">
          <li><a href=\"#rilisan\">Rilisan</a></li>
          <li><a href=\"";
        // line 15
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("garapan");
        echo "\">Garapan</a></li>
          <li><a href=\"";
        // line 16
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("tentang");
        echo "\">Tentang</a></li>
          <li><a href=\"";
        // line 17
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("daftar");
        echo "\">Daftar</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-home/navigation.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 17,  48 => 16,  44 => 15,  37 => 11,  33 => 10,  29 => 9,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"lt-navbar navbar-fixed\">
  <nav class=\"lt-no-background\">
    <div class=\"nav-wrapper\">
      <div class=\"lt-container container\">
        <a href=\"#bittersweet\" class=\"brand-logo white-text\">Caramel</a>
        <a href=\"#\" data-activates=\"mobile-menu\" class=\"lt-mobile-toggle button-collapse\"><i class=\"material-icons\">menu</i></a>
        <ul class=\"right hide-on-med-and-down\">
          <li><a href=\"#rilisan\">Rilisan</a></li>
          <li><a href=\"{{ 'garapan'|page }}\">Garapan</a></li>
          <li><a href=\"{{ 'tentang'|page }}\">Tentang</a></li>
          <li><a href=\"{{ 'daftar'|page }}\">Daftar</a></li>
        </ul>
        <ul class=\"side-nav\" id=\"mobile-menu\">
          <li><a href=\"#rilisan\">Rilisan</a></li>
          <li><a href=\"{{ 'garapan'|page }}\">Garapan</a></li>
          <li><a href=\"{{ 'tentang'|page }}\">Tentang</a></li>
          <li><a href=\"{{ 'daftar'|page }}\">Daftar</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>", "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-home/navigation.htm", "");
    }
}
