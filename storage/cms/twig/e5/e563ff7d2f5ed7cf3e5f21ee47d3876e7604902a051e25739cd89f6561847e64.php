<?php

/* /home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/layouts/octaskin.htm */
class __TwigTemplate_9a7d73c3270a6899474fac7537b32c7960df6f8b663d16c0c9dad91daff992c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    ";
        // line 8
        $context['__placeholder_seo_default_contents'] = null;        ob_start();        // line 9
        echo "
    <meta name=\"description\" content=\"";
        // line 10
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array(), "any", false, true), "meta_description", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array(), "any", false, true), "meta_description", array()), $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "description", array()))) : ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "description", array()))), "html", null, true);
        echo "\">
    <meta name=\"keywords\" content=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "keywords", array()), "html", null, true);
        echo "\">
    <meta name=\"author\" content=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_author", array()), "html", null, true);
        echo "\">

    <meta property=\"og:title\" content=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_name", array()), "html", null, true);
        echo "\" />
    <meta property=\"og:url\" content=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_url", array()), "html", null, true);
        echo "\" />
    <meta property=\"og:type\" content=\"article\" />
    <meta property=\"og:description\" content=\"";
        // line 17
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array(), "any", false, true), "meta_description", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array(), "any", false, true), "meta_description", array()), $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "description", array()))) : ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "description", array()))), "html", null, true);
        echo "\" />
    <!-- <meta property=\"og:image\" content=\"http://momo.bittersweet.id/storage/app/uploads/public/58b/696/510/58b696510c79a743246245.jpg\" /> -->
    <meta name=\"twitter:card\" content=\"summary\" />
    <meta name=\"twitter:title\" content=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_name", array()), "html", null, true);
        echo "\" />
    <meta name=\"twitter:url\" content=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_url", array()), "html", null, true);
        echo "\" />
    <!-- <meta name=\"twitter:image\" content=\"http://momo.bittersweet.id/storage/app/uploads/public/58b/696/510/58b696510c79a743246245.jpg\" /> -->

    <title>";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_name", array()), "html", null, true);
        echo "</title>
    <link rel=\"canonical\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_url", array()), "html", null, true);
        echo "\" />

    ";
        $context['__placeholder_seo_default_contents'] = ob_get_clean();        // line 8
        echo $this->env->getExtension('CMS')->displayBlock('seo', $context['__placeholder_seo_default_contents']);
        unset($context['__placeholder_seo_default_contents']);        // line 28
        echo "
    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 29
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icon.png");
        echo "\" />

    ";
        // line 31
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "load_google_fonts", array())) {
            // line 32
            echo "      <link href=\"https://fonts.googleapis.com/css?family=";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "google_font_family", array()), "html", null, true);
            echo "\" rel=\"stylesheet\">
    ";
        }
        // line 34
        echo "
    ";
        // line 35
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "load_fontawesome", array())) {
            // line 36
            echo "      <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css\" rel=\"stylesheet\">
    ";
        }
        // line 38
        echo "
    ";
        // line 39
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "load_material_icons", array())) {
            // line 40
            echo "      <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
    ";
        }
        // line 42
        echo "
    ";
        // line 43
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "load_stroke7_icons", array())) {
            // line 44
            echo "      <link href=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/stroke7/stroke7-icon-font.min.css");
            echo "\" rel=\"stylesheet\">
    ";
        }
        // line 46
        echo "
    <link href=\"";
        // line 47
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter(array(0 => "assets/scss/style.scss"));
        echo "\" rel=\"stylesheet\">
    
    ";
        // line 49
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("_addons/css"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 50
        echo "
    ";
        // line 51
        echo $this->env->getExtension('CMS')->assetsFunction('css');
        echo $this->env->getExtension('CMS')->displayBlock('styles');
        // line 52
        echo "
    ";
        // line 53
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "custom_css", array())) {
            // line 54
            echo "      <style>
      ";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "custom_css", array()), "html", null, true);
            echo "
      </style>
    ";
        }
        // line 58
        echo "  </head>
  <body class=\"lt-theme-";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "config", array()), "code", array()), "html", null, true);
        echo " lt-layout-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "layout", array()), "id", array()), "html", null, true);
        echo " lt-page-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()), "html", null, true);
        echo "\">
    <div id=\"lt-page-surround\">
      <section id=\"lt-navigation\" class=\"lt-section section lt-no-background\">
        <div class=\"lt-row row\">
          <div class=\"lt-content lt-navigation-content\">
          ";
        // line 64
        if (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()) == "home")) {
            // line 65
            echo "            ";
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('CMS')->partialFunction("pages-home/navigation"            , $context['__cms_partial_params']            );
            unset($context['__cms_partial_params']);
            // line 66
            echo "          ";
        } else {
            // line 67
            echo "            ";
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('CMS')->partialFunction("pages-all/navigation"            , $context['__cms_partial_params']            );
            unset($context['__cms_partial_params']);
            // line 68
            echo "          ";
        }
        // line 69
        echo "          </div>
        </div>
      </section>

      ";
        // line 73
        echo $this->env->getExtension('CMS')->pageFunction();
        // line 74
        echo "
      <!--<section id=\"lt-footer\" class=\"lt-section section\">-->
        <!--<div class=\"lt-container container\">-->
          <!--<div class=\"lt-content lt-footer-content\">-->
            <!--";
        // line 78
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("pages-all/footer"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        echo "-->
          <!--</div>-->
        <!--</div>-->
      <!--</section>-->

      <section id=\"lt-copyright\" class=\"lt-section section\">
        <div class=\"lt-container container\">
          <div class=\"lt-content lt-copyright-content\">
            ";
        // line 86
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("pages-all/copyright"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 87
        echo "          </div>
        </div>
      </section>
    </div>

    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "jQuery_version", array()), "html", null, true);
        echo "/jquery.min.js\"></script>
    <script src=\"";
        // line 93
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter(array(0 => "assets/js/materialize.min.js", 1 => "assets/js/theme.js"));
        echo "\"></script>


    ";
        // line 96
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        // line 97
        echo "    ";
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "load_octobercms_framework", array())) {
            // line 98
            echo "      ";
            echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
            echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
            // line 99
            echo "    ";
        }
        // line 100
        echo "
    ";
        // line 101
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("_addons/js"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 102
        echo "
    ";
        // line 103
        echo $this->env->getExtension('CMS')->assetsFunction('js');
        echo $this->env->getExtension('CMS')->displayBlock('scripts');
        // line 104
        echo "
    ";
        // line 105
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "custom_js", array())) {
            // line 106
            echo "      <script>
      ";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "custom_js", array()), "html", null, true);
            echo "
      </script>
    ";
        }
        // line 110
        echo "  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/layouts/octaskin.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 110,  277 => 107,  274 => 106,  272 => 105,  269 => 104,  266 => 103,  263 => 102,  259 => 101,  256 => 100,  253 => 99,  245 => 98,  242 => 97,  239 => 96,  233 => 93,  229 => 92,  222 => 87,  218 => 86,  205 => 78,  199 => 74,  197 => 73,  191 => 69,  188 => 68,  183 => 67,  180 => 66,  175 => 65,  173 => 64,  161 => 59,  158 => 58,  152 => 55,  149 => 54,  147 => 53,  144 => 52,  141 => 51,  138 => 50,  134 => 49,  129 => 47,  126 => 46,  120 => 44,  118 => 43,  115 => 42,  111 => 40,  109 => 39,  106 => 38,  102 => 36,  100 => 35,  97 => 34,  91 => 32,  89 => 31,  84 => 29,  81 => 28,  79 => 8,  74 => 25,  70 => 24,  64 => 21,  60 => 20,  54 => 17,  49 => 15,  45 => 14,  40 => 12,  36 => 11,  32 => 10,  29 => 9,  28 => 8,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    {% placeholder seo default %}

    <meta name=\"description\" content=\"{{ this.page.meta_description|default(this.theme.description) }}\">
    <meta name=\"keywords\" content=\"{{ this.theme.keywords }}\">
    <meta name=\"author\" content=\"{{ this.theme.website_author }}\">

    <meta property=\"og:title\" content=\"{{ this.theme.website_name }}\" />
    <meta property=\"og:url\" content=\"{{ this.theme.website_url }}\" />
    <meta property=\"og:type\" content=\"article\" />
    <meta property=\"og:description\" content=\"{{ this.page.meta_description|default(this.theme.description) }}\" />
    <!-- <meta property=\"og:image\" content=\"http://momo.bittersweet.id/storage/app/uploads/public/58b/696/510/58b696510c79a743246245.jpg\" /> -->
    <meta name=\"twitter:card\" content=\"summary\" />
    <meta name=\"twitter:title\" content=\"{{ this.theme.website_name }}\" />
    <meta name=\"twitter:url\" content=\"{{ this.theme.website_url }}\" />
    <!-- <meta name=\"twitter:image\" content=\"http://momo.bittersweet.id/storage/app/uploads/public/58b/696/510/58b696510c79a743246245.jpg\" /> -->

    <title>{{ this.theme.website_name }}</title>
    <link rel=\"canonical\" href=\"{{ this.theme.website_url }}\" />

    {% endplaceholder %}

    <link rel=\"icon\" type=\"image/png\" href=\"{{ 'assets/img/icon.png'|theme }}\" />

    {% if this.theme.load_google_fonts %}
      <link href=\"https://fonts.googleapis.com/css?family={{ this.theme.google_font_family }}\" rel=\"stylesheet\">
    {% endif %}

    {% if this.theme.load_fontawesome %}
      <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css\" rel=\"stylesheet\">
    {% endif %}

    {% if this.theme.load_material_icons %}
      <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
    {% endif %}

    {% if this.theme.load_stroke7_icons %}
      <link href=\"{{ 'assets/css/stroke7/stroke7-icon-font.min.css'|theme }}\" rel=\"stylesheet\">
    {% endif %}

    <link href=\"{{ ['assets/scss/style.scss']|theme }}\" rel=\"stylesheet\">
    
    {% partial \"_addons/css\" %}

    {% styles %}

    {% if this.theme.custom_css %}
      <style>
      {{ this.theme.custom_css }}
      </style>
    {% endif %}
  </head>
  <body class=\"lt-theme-{{ this.theme.config.code }} lt-layout-{{ this.layout.id }} lt-page-{{ this.page.id }}\">
    <div id=\"lt-page-surround\">
      <section id=\"lt-navigation\" class=\"lt-section section lt-no-background\">
        <div class=\"lt-row row\">
          <div class=\"lt-content lt-navigation-content\">
          {% if this.page.id == 'home' %}
            {% partial \"pages-home/navigation\" %}
          {% else %}
            {% partial \"pages-all/navigation\" %}
          {% endif %}
          </div>
        </div>
      </section>

      {% page %}

      <!--<section id=\"lt-footer\" class=\"lt-section section\">-->
        <!--<div class=\"lt-container container\">-->
          <!--<div class=\"lt-content lt-footer-content\">-->
            <!--{% partial \"pages-all/footer\" %}-->
          <!--</div>-->
        <!--</div>-->
      <!--</section>-->

      <section id=\"lt-copyright\" class=\"lt-section section\">
        <div class=\"lt-container container\">
          <div class=\"lt-content lt-copyright-content\">
            {% partial \"pages-all/copyright\" %}
          </div>
        </div>
      </section>
    </div>

    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/{{ this.theme.jQuery_version }}/jquery.min.js\"></script>
    <script src=\"{{ ['assets/js/materialize.min.js','assets/js/theme.js']|theme }}\"></script>


    {% framework %}
    {% if this.theme.load_octobercms_framework %}
      {% framework extras %}
    {% endif %}

    {% partial \"_addons/js\" %}

    {% scripts %}

    {% if this.theme.custom_js %}
      <script>
      {{ this.theme.custom_js }}
      </script>
    {% endif %}
  </body>
</html>", "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/layouts/octaskin.htm", "");
    }
}
