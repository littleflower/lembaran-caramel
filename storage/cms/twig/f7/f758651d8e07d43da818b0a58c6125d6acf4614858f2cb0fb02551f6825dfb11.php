<?php

/* /home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-kabar/content.htm */
class __TwigTemplate_671c33e7f03e443f49c727410e8966ca167e065ab5841db2b82ea90a310c7adc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <h2 class=\"lt-title\">Kabar</h2>
            <p class=\"flow-text\">Mungkin saja garapan yang kamu tunggu sudah rilis. ~</p>
        </div>
    </div>
</div>
<div class=\"lt-vertical-spacer\"></div>

";
        // line 11
        $context["posts"] = $this->getAttribute(($context["blogPosts"] ?? null), "posts", array());
        // line 12
        echo "
";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 14
            echo "
<div class=\"lt-row row\">
    <div class=\"lt-col col m12 s12\">
        <div class=\"card z-depth-2\" id=\"card-kabar\">
            <div class=\"row\">
                <div class=\"lt-col col m3 s12\">
                    <div class=\"card-image\">
                        <a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\">
                            ";
            // line 22
            if ($this->getAttribute($this->getAttribute($context["post"], "featured_images", array()), "count", array())) {
                // line 23
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute($context["post"], "featured_images", array()), 0, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 24
                    echo "                            <img class=\"responsive-img\" data-src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename", array()), "html", null, true);
                    echo "\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "path", array()), "html", null, true);
                    echo "\"
                                 alt=\"";
                    // line 25
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "description", array()) != null)) ? ($this->getAttribute($context["image"], "description", array())) : ($this->getAttribute($context["image"], "filename", array()))), "html", null, true);
                    echo "\"
                                 title=\"";
                    // line 26
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "title", array()) != null)) ? ($this->getAttribute($context["image"], "title", array())) : ($this->getAttribute($context["post"], "title", array()))), "html", null, true);
                    echo "\">
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 28
                echo "                            ";
            } else {
                // line 29
                echo "                            <img class=\"responsive-img circle-img\" alt=\"image\"
                                 src=\"";
                // line 30
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/pages/1701/header-404.jpg");
                echo "\">
                            ";
            }
            // line 32
            echo "                        </a>
                    </div>
                </div>
                <div class=\"lt-col col m9 s12\">
                    <div class=\"card-content\">
                        <span class=\"right small\">";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["post"], "published_at", array()), "M d, Y"), "html", null, true);
            echo "</span>
                        <span class=\"card-title\"><a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</span></a>
                        <br/>
                        <p>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "summary", array()), "html", null, true);
            echo "</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 49
            echo "<div class=\"lt-vertical-spacer\"></div>
<div class=\"lt-vertical-spacer\"></div>
<h5><center class=\"align-center center\">";
            // line 51
            echo twig_escape_filter($this->env, ($context["noPostsMessage"] ?? null), "html", null, true);
            echo "</center></h5>
<div class=\"lt-vertical-spacer\"></div>
<div class=\"lt-vertical-spacer\"></div>
<div class=\"lt-vertical-spacer\"></div>
<div class=\"lt-vertical-spacer\"></div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "
<div class=\"lt-vertical-spacer\"></div>

<span class=\"center-align\">
";
        // line 61
        if (($this->getAttribute(($context["posts"] ?? null), "lastPage", array()) > 1)) {
            // line 62
            echo "<ul class=\"pagination\">
    ";
            // line 63
            if (($this->getAttribute(($context["posts"] ?? null), "currentPage", array()) > 1)) {
                // line 64
                echo "    <li><a href=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => ($this->getAttribute(($context["posts"] ?? null), "currentPage", array()) - 1)));
                echo "\">&larr; Prev</a></li>
    ";
            }
            // line 66
            echo "
    ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute(($context["posts"] ?? null), "lastPage", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 68
                echo "    <li class=\"";
                echo ((($this->getAttribute(($context["posts"] ?? null), "currentPage", array()) == $context["page"])) ? ("active") : (null));
                echo "\">
        <a href=\"";
                // line 69
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => $context["page"]));
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
    </li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 72
            echo "
    ";
            // line 73
            if (($this->getAttribute(($context["posts"] ?? null), "lastPage", array()) > $this->getAttribute(($context["posts"] ?? null), "currentPage", array()))) {
                // line 74
                echo "    <li><a href=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => ($this->getAttribute(($context["posts"] ?? null), "currentPage", array()) + 1)));
                echo "\">Next &rarr;</a></li>
    ";
            }
            // line 76
            echo "</ul>
";
        }
        // line 78
        echo "</span>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-kabar/content.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 78,  192 => 76,  186 => 74,  184 => 73,  181 => 72,  170 => 69,  165 => 68,  161 => 67,  158 => 66,  152 => 64,  150 => 63,  147 => 62,  145 => 61,  139 => 57,  127 => 51,  123 => 49,  109 => 40,  102 => 38,  98 => 37,  91 => 32,  86 => 30,  83 => 29,  80 => 28,  72 => 26,  68 => 25,  61 => 24,  56 => 23,  54 => 22,  50 => 21,  41 => 14,  36 => 13,  33 => 12,  31 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <h2 class=\"lt-title\">Kabar</h2>
            <p class=\"flow-text\">Mungkin saja garapan yang kamu tunggu sudah rilis. ~</p>
        </div>
    </div>
</div>
<div class=\"lt-vertical-spacer\"></div>

{% set posts = blogPosts.posts %}

{% for post in posts %}

<div class=\"lt-row row\">
    <div class=\"lt-col col m12 s12\">
        <div class=\"card z-depth-2\" id=\"card-kabar\">
            <div class=\"row\">
                <div class=\"lt-col col m3 s12\">
                    <div class=\"card-image\">
                        <a href=\"{{ post.url }}\">
                            {% if post.featured_images.count %}
                            {% for image in post.featured_images|slice(0, 1) %}
                            <img class=\"responsive-img\" data-src=\"{{ image.filename }}\" src=\"{{ image.path }}\"
                                 alt=\"{{ image.description != null ? image.description : image.filename }}\"
                                 title=\"{{ image.title != null ? image.title : post.title }}\">
                            {% endfor %}
                            {% else %}
                            <img class=\"responsive-img circle-img\" alt=\"image\"
                                 src=\"{{ 'assets/img/pages/1701/header-404.jpg'|theme }}\">
                            {% endif %}
                        </a>
                    </div>
                </div>
                <div class=\"lt-col col m9 s12\">
                    <div class=\"card-content\">
                        <span class=\"right small\">{{ post.published_at|date('M d, Y') }}</span>
                        <span class=\"card-title\"><a href=\"{{ post.url }}\">{{ post.title }}</span></a>
                        <br/>
                        <p>{{ post.summary }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{% else %}
<div class=\"lt-vertical-spacer\"></div>
<div class=\"lt-vertical-spacer\"></div>
<h5><center class=\"align-center center\">{{ noPostsMessage }}</center></h5>
<div class=\"lt-vertical-spacer\"></div>
<div class=\"lt-vertical-spacer\"></div>
<div class=\"lt-vertical-spacer\"></div>
<div class=\"lt-vertical-spacer\"></div>
{% endfor %}

<div class=\"lt-vertical-spacer\"></div>

<span class=\"center-align\">
{% if posts.lastPage > 1 %}
<ul class=\"pagination\">
    {% if posts.currentPage > 1 %}
    <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (posts.currentPage-1) }) }}\">&larr; Prev</a></li>
    {% endif %}

    {% for page in 1..posts.lastPage %}
    <li class=\"{{ posts.currentPage == page ? 'active' : null }}\">
        <a href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}\">{{ page }}</a>
    </li>
    {% endfor %}

    {% if posts.lastPage > posts.currentPage %}
    <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (posts.currentPage+1) }) }}\">Next &rarr;</a></li>
    {% endif %}
</ul>
{% endif %}
</span>", "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-kabar/content.htm", "");
    }
}
