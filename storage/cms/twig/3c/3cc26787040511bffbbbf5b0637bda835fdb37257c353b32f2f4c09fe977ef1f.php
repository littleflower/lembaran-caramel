<?php

/* /home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/tentang.htm */
class __TwigTemplate_932c6e143ced51336c7d616b146fd40182ab3e25b4d2d8e976d6f323c2ee665f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <div class=\"card z-depth-2 collection with-header\">
                <div class=\"collection-header\">
                    <p/>
                    <h2 class=\"lt-title\">Tentang</h2>
                    <p class=\"flow-text\">Mau tahu banget? Huh, kepo!</p>
                </div>
                <div class=\"card-image\">
                    <img src=\"http://caramel.id/storage/app/media/gumi.jpg\"/>
                </div>
                <div class=\"card-content\">
                    <p>Caramel, hasil peleburan dua fansub yang sebelumnya menjalin persahabatan yang sangat erat: Attakai dan Bitter ❤ Sweet. Berangkat dari kesamaan nasib dan visi, kami sepakat untuk tinggal di atas satu atap. Menyatukan dua keluarga layaknya sepasang kekasih. Membina rumah tangga dengan penuh kehangatan cinta yang terasa manis.</p><br>
                    <p>Berawal dari inisiatif littleflower untuk menyatukan fansub yang sedang dipegangnya, Bitter ❤ Sweet dengan sahabat karibnya, Attakai. Tanpa ada keraguan, Nail sebagai wali dari Attakai menyetujuinya. Inisiatif itu terwujud atas bantuan Ryesuu sebagai mediator (karena dia staff di Attakai sekaligus Bitter ❤ Sweet) pada pertengahan Februari, disaat masing-masing dari mereka juga terlibat dalam proyek yang sama, yang sampai saat tulisan ini diketik (6 April 2017, red.) pun masih belum selesai.</p><br>
                    <p>Proses penyatuan berjalan lancar. Bisa dikatakan, tidak ada kendala. Terkecuali satu, \"molor\" dari rencana awal. Ya, itu sudah biasa. Namun yap, kami setuju kalau kami harus mengurangi kebiasaan yang buruk itu. Awalnya kami berencana mengumumkannya persis 1 April. Namun dengan dalih \"April Mop\" (padahal memang masih ada persiapan yang belum selesai), rencana tersebut diundur hingga ada satu garapan musim ini (musim semi 2017) yang bisa dirilis. Dan saat itu akhirnya tiba, dipenghujung hari ini, Kamis, 6 April 2017, kami siap mengumumkan bahwa Attakai dan Bitter ❤ Sweet sudah tidak ada, melebur menjadi Caramel.</p><br>
                    <p>Nama \"Caramel\" dipilih secara demokratis oleh semua anggota. Menang satu putaran dengan selisih tipis dari kandidat nama lainnya. Nama yang terdengar manis, bukan?</p><br>
                    <p>Kamu masih membaca hingga bagian ini? Selamat, kamu sudah membuang waktu untuk membaca tulisan yang tidak berguna bagi hidupmu. Namun, kami ucapkan banyak terima kasih telah menyempatkan waktunya untuk lebih mengenal kami!</p><br>
                    <p>Terus dukung kami, ya! Jangan lupa like fanspage dan gabung di channel discord kami! ❤</p>
                </div>
                <div class=\"card-action\">
                    <a href=\"https://www.facebook.com/caramelfansub/\" target=\"_blank\" class=\"btn-floating btn-large waves-effect waves-light blue darken-4\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a>
                    <a href=\"https://discord.gg/7zQeP3\" target=\"_blank\" class=\"btn-floating btn-large waves-effect waves-light deep-purple\"><i class=\"fa fa-gamepad\" aria-hidden=\"true\"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class=\"lt-vertical-spacer\"></div>

<div class=\"card z-depth-2\">
    
    
</div>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/tentang.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <div class=\"card z-depth-2 collection with-header\">
                <div class=\"collection-header\">
                    <p/>
                    <h2 class=\"lt-title\">Tentang</h2>
                    <p class=\"flow-text\">Mau tahu banget? Huh, kepo!</p>
                </div>
                <div class=\"card-image\">
                    <img src=\"http://caramel.id/storage/app/media/gumi.jpg\"/>
                </div>
                <div class=\"card-content\">
                    <p>Caramel, hasil peleburan dua fansub yang sebelumnya menjalin persahabatan yang sangat erat: Attakai dan Bitter ❤ Sweet. Berangkat dari kesamaan nasib dan visi, kami sepakat untuk tinggal di atas satu atap. Menyatukan dua keluarga layaknya sepasang kekasih. Membina rumah tangga dengan penuh kehangatan cinta yang terasa manis.</p><br>
                    <p>Berawal dari inisiatif littleflower untuk menyatukan fansub yang sedang dipegangnya, Bitter ❤ Sweet dengan sahabat karibnya, Attakai. Tanpa ada keraguan, Nail sebagai wali dari Attakai menyetujuinya. Inisiatif itu terwujud atas bantuan Ryesuu sebagai mediator (karena dia staff di Attakai sekaligus Bitter ❤ Sweet) pada pertengahan Februari, disaat masing-masing dari mereka juga terlibat dalam proyek yang sama, yang sampai saat tulisan ini diketik (6 April 2017, red.) pun masih belum selesai.</p><br>
                    <p>Proses penyatuan berjalan lancar. Bisa dikatakan, tidak ada kendala. Terkecuali satu, \"molor\" dari rencana awal. Ya, itu sudah biasa. Namun yap, kami setuju kalau kami harus mengurangi kebiasaan yang buruk itu. Awalnya kami berencana mengumumkannya persis 1 April. Namun dengan dalih \"April Mop\" (padahal memang masih ada persiapan yang belum selesai), rencana tersebut diundur hingga ada satu garapan musim ini (musim semi 2017) yang bisa dirilis. Dan saat itu akhirnya tiba, dipenghujung hari ini, Kamis, 6 April 2017, kami siap mengumumkan bahwa Attakai dan Bitter ❤ Sweet sudah tidak ada, melebur menjadi Caramel.</p><br>
                    <p>Nama \"Caramel\" dipilih secara demokratis oleh semua anggota. Menang satu putaran dengan selisih tipis dari kandidat nama lainnya. Nama yang terdengar manis, bukan?</p><br>
                    <p>Kamu masih membaca hingga bagian ini? Selamat, kamu sudah membuang waktu untuk membaca tulisan yang tidak berguna bagi hidupmu. Namun, kami ucapkan banyak terima kasih telah menyempatkan waktunya untuk lebih mengenal kami!</p><br>
                    <p>Terus dukung kami, ya! Jangan lupa like fanspage dan gabung di channel discord kami! ❤</p>
                </div>
                <div class=\"card-action\">
                    <a href=\"https://www.facebook.com/caramelfansub/\" target=\"_blank\" class=\"btn-floating btn-large waves-effect waves-light blue darken-4\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a>
                    <a href=\"https://discord.gg/7zQeP3\" target=\"_blank\" class=\"btn-floating btn-large waves-effect waves-light deep-purple\"><i class=\"fa fa-gamepad\" aria-hidden=\"true\"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class=\"lt-vertical-spacer\"></div>

<div class=\"card z-depth-2\">
    
    
</div>", "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/tentang.htm", "");
    }
}
