<?php

/* /home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/garapan.htm */
class __TwigTemplate_9dd5dc63939d95b66ea5ce0ffa554a22672f84cf71088645b29eb90ccbdc24b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <h2 class=\"lt-title\">Garapan</h2>
            <p class=\"flow-text\">Garapan spesial buat kamu yang spesial ❤</p>
            <p>Garapan lainnya bisa kalian lihat di <a href=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("garapan");
        echo "\">sini</a>.</p>
        </div>
    </div>
</div>

<div class=\"lt-vertical-spacer\"></div>

";
        // line 13
        $context["posts"] = $this->getAttribute(($context["blogPosts"] ?? null), "posts", array());
        // line 14
        echo "
<div class=\"lt-row row\">

    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 18
            echo "
    <div class=\"lt-col col m6 s12\">

        <div class=\"card large z-depth-2\">
            <div class=\"card-image\">
                <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\">
                    ";
            // line 24
            if ($this->getAttribute($this->getAttribute($context["post"], "featured_images", array()), "count", array())) {
                // line 25
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute($context["post"], "featured_images", array()), 1, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 26
                    echo "                    <img class=\"responsive-img activator\" data-src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename", array()), "html", null, true);
                    echo "\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "path", array()), "html", null, true);
                    echo "\"
                         alt=\"";
                    // line 27
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "description", array()) != null)) ? ($this->getAttribute($context["image"], "description", array())) : ($this->getAttribute($context["image"], "filename", array()))), "html", null, true);
                    echo "\"
                         title=\"";
                    // line 28
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "title", array()) != null)) ? ($this->getAttribute($context["image"], "title", array())) : ($this->getAttribute($context["post"], "title", array()))), "html", null, true);
                    echo "\">
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "                    ";
            } else {
                // line 31
                echo "                    <img class=\"responsive-img activator\" alt=\"image\"
                         src=\"";
                // line 32
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/pages/1701/header-404.jpg");
                echo "\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
                echo "\">
                    ";
            }
            // line 34
            echo "                </a>
                <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\" class=\"btn-floating btn-large halfway-fab waves-effect waves-light right pink\"
                   style=\"margin-top: -32px; margin-right: 32px\">
                    <i class=\"material-icons\">launch</i></a>
            </div>
            <div class=\"card-content\">
                ";
            // line 40
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["post"], "categories", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 41
                echo "                    ";
                if (($this->getAttribute($context["category"], "name", array()) == "Garapan Selesai")) {
                    // line 42
                    echo "                        <span class=\"chip green\" style=\"color: white; font-family: Open Sans;\">Selesai</span>
                    ";
                } elseif (($this->getAttribute(                // line 43
$context["category"], "name", array()) == "Garapan Lancar")) {
                    // line 44
                    echo "                        <span class=\"chip blue\" style=\"color: white; font-family: Open Sans;\">Lancar</span>
                    ";
                } elseif (($this->getAttribute(                // line 45
$context["category"], "name", array()) == "Garapan Terhambat")) {
                    // line 46
                    echo "                        <span class=\"chip yellow darken-3\" style=\"color: white; font-family: Open Sans;\">Lancar?</span>
                    ";
                } elseif (($this->getAttribute(                // line 47
$context["category"], "name", array()) == "Garapan Batal")) {
                    // line 48
                    echo "                        <span class=\"chip red\" style=\"color: white; font-family: Open Sans;\">Batal</span>
                    ";
                } elseif (($this->getAttribute(                // line 49
$context["category"], "name", array()) == "Sedang Tayang")) {
                    // line 50
                    echo "                        <span class=\"chip purple\" style=\"color: white; font-family: Open Sans;\">Tayang</span>
                    ";
                } elseif (call_user_func_array($this->env->getFunction('str_contains')->getCallable(), array("contains", $this->getAttribute(                // line 51
$context["category"], "slug", array()), "join-"))) {
                    // line 52
                    echo "                        <span class=\"chip pink\" style=\"color: white; font-family: Open Sans;\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                    echo "</span>
                    ";
                }
                // line 54
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "                <br>
                <span class=\"card-title\">";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</span>
                <p>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "summary", array()), "html", null, true);
            echo "</p>
            </div>
        </div>

    </div>

    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 64
            echo "    <div class=\"lt-vertical-spacer\"></div>
    <div class=\"lt-vertical-spacer\"></div>
    <h5><center class=\"align-center center\">";
            // line 66
            echo twig_escape_filter($this->env, ($context["noPostsMessage"] ?? null), "html", null, true);
            echo "</center></h5>
    <div class=\"lt-vertical-spacer\"></div>
    <div class=\"lt-vertical-spacer\"></div>
    <div class=\"lt-vertical-spacer\"></div>
    <div class=\"lt-vertical-spacer\"></div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/garapan.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 72,  176 => 66,  172 => 64,  160 => 57,  156 => 56,  153 => 55,  147 => 54,  141 => 52,  139 => 51,  136 => 50,  134 => 49,  131 => 48,  129 => 47,  126 => 46,  124 => 45,  121 => 44,  119 => 43,  116 => 42,  113 => 41,  109 => 40,  101 => 35,  98 => 34,  91 => 32,  88 => 31,  85 => 30,  77 => 28,  73 => 27,  66 => 26,  61 => 25,  59 => 24,  55 => 23,  48 => 18,  43 => 17,  38 => 14,  36 => 13,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <h2 class=\"lt-title\">Garapan</h2>
            <p class=\"flow-text\">Garapan spesial buat kamu yang spesial ❤</p>
            <p>Garapan lainnya bisa kalian lihat di <a href=\"{{ 'garapan'|page }}\">sini</a>.</p>
        </div>
    </div>
</div>

<div class=\"lt-vertical-spacer\"></div>

{% set posts = blogPosts.posts %}

<div class=\"lt-row row\">

    {% for post in posts %}

    <div class=\"lt-col col m6 s12\">

        <div class=\"card large z-depth-2\">
            <div class=\"card-image\">
                <a href=\"{{ post.url }}\">
                    {% if post.featured_images.count %}
                    {% for image in post.featured_images|slice(1, 1) %}
                    <img class=\"responsive-img activator\" data-src=\"{{ image.filename }}\" src=\"{{ image.path }}\"
                         alt=\"{{ image.description != null ? image.description : image.filename }}\"
                         title=\"{{ image.title != null ? image.title : post.title }}\">
                    {% endfor %}
                    {% else %}
                    <img class=\"responsive-img activator\" alt=\"image\"
                         src=\"{{ 'assets/img/pages/1701/header-404.jpg'|theme }}\" href=\"{{ post.url }}\">
                    {% endif %}
                </a>
                <a href=\"{{ post.url }}\" class=\"btn-floating btn-large halfway-fab waves-effect waves-light right pink\"
                   style=\"margin-top: -32px; margin-right: 32px\">
                    <i class=\"material-icons\">launch</i></a>
            </div>
            <div class=\"card-content\">
                {% for category in post.categories %}
                    {% if category.name == \"Garapan Selesai\" %}
                        <span class=\"chip green\" style=\"color: white; font-family: Open Sans;\">Selesai</span>
                    {% elseif category.name == \"Garapan Lancar\" %}
                        <span class=\"chip blue\" style=\"color: white; font-family: Open Sans;\">Lancar</span>
                    {% elseif category.name == \"Garapan Terhambat\" %}
                        <span class=\"chip yellow darken-3\" style=\"color: white; font-family: Open Sans;\">Lancar?</span>
                    {% elseif category.name == \"Garapan Batal\" %}
                        <span class=\"chip red\" style=\"color: white; font-family: Open Sans;\">Batal</span>
                    {% elseif category.name == \"Sedang Tayang\" %}
                        <span class=\"chip purple\" style=\"color: white; font-family: Open Sans;\">Tayang</span>
                    {% elseif str_contains(category.slug, 'join-') %}
                        <span class=\"chip pink\" style=\"color: white; font-family: Open Sans;\">{{ category.name }}</span>
                    {% endif %}
                {% endfor %}
                <br>
                <span class=\"card-title\">{{ post.title }}</span>
                <p>{{ post.summary }}</p>
            </div>
        </div>

    </div>

    {% else %}
    <div class=\"lt-vertical-spacer\"></div>
    <div class=\"lt-vertical-spacer\"></div>
    <h5><center class=\"align-center center\">{{ noPostsMessage }}</center></h5>
    <div class=\"lt-vertical-spacer\"></div>
    <div class=\"lt-vertical-spacer\"></div>
    <div class=\"lt-vertical-spacer\"></div>
    <div class=\"lt-vertical-spacer\"></div>
    {% endfor %}

</div>", "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/garapan.htm", "");
    }
}
