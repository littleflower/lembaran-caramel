<?php

/* /home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-home/slideshow.htm */
class __TwigTemplate_acf42b3638637df6bf15906c53ab1d09a73fe769ee122083787c79b5f7a83f9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["posts"] = $this->getAttribute(($context["blogPosts"] ?? null), "posts", array());
        // line 2
        echo "
<div class=\"lt-row row\">
  <div class=\"lt-col col s12\">
    <div class=\"lt-slider slider fullscreen\">
      <ul class=\"slides\">
        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 8
            echo "        <li>
          ";
            // line 9
            if ($this->getAttribute($this->getAttribute($context["post"], "featured_images", array()), "count", array())) {
                // line 10
                echo "          ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute($context["post"], "featured_images", array()), 1, 2));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 11
                    echo "          <img data-src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename", array()), "html", null, true);
                    echo "\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "path", array()), "html", null, true);
                    echo "\"
               alt=\"";
                    // line 12
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "description", array()) != null)) ? ($this->getAttribute($context["image"], "description", array())) : ($this->getAttribute($context["image"], "filename", array()))), "html", null, true);
                    echo "\"
               title=\"";
                    // line 13
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "title", array()) != null)) ? ($this->getAttribute($context["image"], "title", array())) : ($this->getAttribute($context["post"], "title", array()))), "html", null, true);
                    echo "\">
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 15
                echo "          ";
            } else {
                // line 16
                echo "            <img alt=\"image\" src=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/pages/1701/header-404.jpg");
                echo "\">
          ";
            }
            // line 18
            echo "          <div class=\"caption center-align\">
            <h2 class=\"lt-slider-title\">";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</h2>
            <h4 class=\"lt-slider-subtitle\" style=\"line-height: 2rem;\">";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "summary", array()), "html", null, true);
            echo "</h4>
            <!-- <div class=\"lt-slider-button\">
              <a href=\"https://octobercms.com/theme/laratify-octobercms-octaskin\" target=\"_blank\" class=\"waves-effect waves-light btn-large primary-color-background\">Download<i class=\"material-icons right\">input</i></a>
            </div> -->
            <div class=\"lt-slider-links\">
              <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\">Selengkapnya</a>
            </div>
          </div>
        </li>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 30
            echo "        <li>
          <img src=\"";
            // line 31
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/pages/1701/header-404.jpg");
            echo "\" alt=\"image\">
          <div class=\"caption center-align\">
            <h2 class=\"lt-slider-title\">";
            // line 33
            echo twig_escape_filter($this->env, ($context["noPostsMessage"] ?? null), "html", null, true);
            echo "</h2>
          </div>
        </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "      </ul>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-home/slideshow.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 37,  104 => 33,  99 => 31,  96 => 30,  86 => 25,  78 => 20,  74 => 19,  71 => 18,  65 => 16,  62 => 15,  54 => 13,  50 => 12,  43 => 11,  38 => 10,  36 => 9,  33 => 8,  28 => 7,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set posts = blogPosts.posts %}

<div class=\"lt-row row\">
  <div class=\"lt-col col s12\">
    <div class=\"lt-slider slider fullscreen\">
      <ul class=\"slides\">
        {% for post in posts %}
        <li>
          {% if post.featured_images.count %}
          {% for image in post.featured_images|slice(1, 2) %}
          <img data-src=\"{{ image.filename }}\" src=\"{{ image.path }}\"
               alt=\"{{ image.description != null ? image.description : image.filename }}\"
               title=\"{{ image.title != null ? image.title : post.title }}\">
          {% endfor %}
          {% else %}
            <img alt=\"image\" src=\"{{ 'assets/img/pages/1701/header-404.jpg'|theme }}\">
          {% endif %}
          <div class=\"caption center-align\">
            <h2 class=\"lt-slider-title\">{{ post.title }}</h2>
            <h4 class=\"lt-slider-subtitle\" style=\"line-height: 2rem;\">{{ post.summary }}</h4>
            <!-- <div class=\"lt-slider-button\">
              <a href=\"https://octobercms.com/theme/laratify-octobercms-octaskin\" target=\"_blank\" class=\"waves-effect waves-light btn-large primary-color-background\">Download<i class=\"material-icons right\">input</i></a>
            </div> -->
            <div class=\"lt-slider-links\">
              <a href=\"{{ post.url }}\">Selengkapnya</a>
            </div>
          </div>
        </li>
        {% else %}
        <li>
          <img src=\"{{ 'assets/img/pages/1701/header-404.jpg'|theme }}\" alt=\"image\">
          <div class=\"caption center-align\">
            <h2 class=\"lt-slider-title\">{{ noPostsMessage }}</h2>
          </div>
        </li>
        {% endfor %}
      </ul>
    </div>
  </div>
</div>", "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-home/slideshow.htm", "");
    }
}
