<?php

/* /home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/_addons/js.htm */
class __TwigTemplate_df393d0dd37037104249c52ee977793b4728b7545d03721fe7f85f527882043f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "load_wow_js", array())) {
            // line 2
            echo "  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/graingert-wow/1.2.0/wow.min.js\"></script>
";
        }
        // line 4
        if ($this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "load_owl_carousel", array())) {
            // line 5
            echo "  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/owl.carousel.min.js\"></script>
";
        }
        // line 7
        echo "  <script>
      \$(document).ready(function(){
          \$('.scrollspy').scrollSpy({scrollOffset: 0});
          Materialize.toast('<span>Selamat datang di Lembaran Caramel 2.0 beta.<br>Kalau kamu menemukan bug, segera lapor ke fanspage/discord. <small><i class=\"fa fa-heart\" aria-hidden=\"true\"></i></small></span>', 5000);
      });
  </script>
  <script id=\"cid0020000153001919773\" data-cfasync=\"false\" async src=\"//st.chatango.com/js/gz/emb.js\" style=\"width: 300px;height: 400px;\">{\"handle\":\"nr-chan\",\"arch\":\"js\",\"styles\":{\"a\":\"ff9900\",\"b\":100,\"c\":\"000000\",\"d\":\"000000\",\"k\":\"ff9900\",\"l\":\"ff9900\",\"m\":\"ff9900\",\"p\":\"10\",\"q\":\"ff9900\",\"r\":100,\"pos\":\"br\",\"cv\":1,\"cvbg\":\"ff9900\",\"cvw\":300,\"cvh\":30,\"ticker\":1,\"fwtickm\":1}}</script>
  <script src=\"https://cdn.plyr.io/2.0.12/plyr.js\"></script>
  <script>plyr.setup();</script>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/_addons/js.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 7,  27 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if this.theme.load_wow_js %}
  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/graingert-wow/1.2.0/wow.min.js\"></script>
{% endif %}
{% if this.theme.load_owl_carousel %}
  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/owl.carousel.min.js\"></script>
{% endif %}
  <script>
      \$(document).ready(function(){
          \$('.scrollspy').scrollSpy({scrollOffset: 0});
          Materialize.toast('<span>Selamat datang di Lembaran Caramel 2.0 beta.<br>Kalau kamu menemukan bug, segera lapor ke fanspage/discord. <small><i class=\"fa fa-heart\" aria-hidden=\"true\"></i></small></span>', 5000);
      });
  </script>
  <script id=\"cid0020000153001919773\" data-cfasync=\"false\" async src=\"//st.chatango.com/js/gz/emb.js\" style=\"width: 300px;height: 400px;\">{\"handle\":\"nr-chan\",\"arch\":\"js\",\"styles\":{\"a\":\"ff9900\",\"b\":100,\"c\":\"000000\",\"d\":\"000000\",\"k\":\"ff9900\",\"l\":\"ff9900\",\"m\":\"ff9900\",\"p\":\"10\",\"q\":\"ff9900\",\"r\":100,\"pos\":\"br\",\"cv\":1,\"cvbg\":\"ff9900\",\"cvw\":300,\"cvh\":30,\"ticker\":1,\"fwtickm\":1}}</script>
  <script src=\"https://cdn.plyr.io/2.0.12/plyr.js\"></script>
  <script>plyr.setup();</script>", "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/_addons/js.htm", "");
    }
}
