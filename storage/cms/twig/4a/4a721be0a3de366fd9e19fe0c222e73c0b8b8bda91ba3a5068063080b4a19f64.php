<?php

/* /home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-all/navigation.htm */
class __TwigTemplate_2a955d7dc58bf2e79fb977334063482bf5670c522ff45e29d8a749377a3562c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"lt-navbar navbar-fixed\">
  <nav class=\"lt-no-background\">
    <div class=\"nav-wrapper\">
      <div class=\"lt-container container\">
        <a href=\"";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("home");
        echo "\" class=\"brand-logo white-text\">Caramel</a>
        <a href=\"#\" data-activates=\"mobile-menu\" class=\"lt-mobile-toggle button-collapse\"><i class=\"material-icons\">menu</i></a>
        <ul class=\"right hide-on-med-and-down\">
          <li><a href=\"/rilisan/default\">Rilisan</a></li>
          <li><a href=\"/garapan/default\">Garapan</a></li>
          <li><a href=\"/tentang\">Tentang</a></li>
          <li><a href=\"/daftar\">Daftar</a></li>
        </ul>
        <ul class=\"side-nav\" id=\"mobile-menu\">
          <li><a href=\"/rilisan/default\">Rilisan</a></li>
          <li><a href=\"/garapan/default\">Garapan</a></li>
          <li><a href=\"/tentang\">Tentang</a></li>
          <li><a href=\"/daftar\">Daftar</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-all/navigation.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"lt-navbar navbar-fixed\">
  <nav class=\"lt-no-background\">
    <div class=\"nav-wrapper\">
      <div class=\"lt-container container\">
        <a href=\"{{ 'home'|page }}\" class=\"brand-logo white-text\">Caramel</a>
        <a href=\"#\" data-activates=\"mobile-menu\" class=\"lt-mobile-toggle button-collapse\"><i class=\"material-icons\">menu</i></a>
        <ul class=\"right hide-on-med-and-down\">
          <li><a href=\"/rilisan/default\">Rilisan</a></li>
          <li><a href=\"/garapan/default\">Garapan</a></li>
          <li><a href=\"/tentang\">Tentang</a></li>
          <li><a href=\"/daftar\">Daftar</a></li>
        </ul>
        <ul class=\"side-nav\" id=\"mobile-menu\">
          <li><a href=\"/rilisan/default\">Rilisan</a></li>
          <li><a href=\"/garapan/default\">Garapan</a></li>
          <li><a href=\"/tentang\">Tentang</a></li>
          <li><a href=\"/daftar\">Daftar</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>", "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/partials/pages-all/navigation.htm", "");
    }
}
