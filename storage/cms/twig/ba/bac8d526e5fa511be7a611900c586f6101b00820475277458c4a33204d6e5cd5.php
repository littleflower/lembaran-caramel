<?php

/* /home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/pages/user.htm */
class __TwigTemplate_86181638d47180772d7ea67ddd336553e2695f01e85b02f09ce45ec4623a13b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"lt-header\" class=\"lt-section lt-section-fullwidth section\">
  <div class=\"lt-container container\">
    <div class=\"lt-content lt-header-content\">
      ";
        // line 4
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("pages-tentang/header"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 5
        echo "    </div>
  </div>
</section>

<section id=\"lt-mainpage\" class=\"lt-section section orange lighten-5\">
  <div class=\"lt-container container\">
    <div class=\"lt-content lt-mainpage-content\">
      <div class=\"card z-depth-2\" style=\"padding:36px\">
        ";
        // line 13
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("account"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 14
        echo "      </div>
    </div>
  </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/pages/user.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 14,  38 => 13,  28 => 5,  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"lt-header\" class=\"lt-section lt-section-fullwidth section\">
  <div class=\"lt-container container\">
    <div class=\"lt-content lt-header-content\">
      {% partial \"pages-tentang/header\" %}
    </div>
  </div>
</section>

<section id=\"lt-mainpage\" class=\"lt-section section orange lighten-5\">
  <div class=\"lt-container container\">
    <div class=\"lt-content lt-mainpage-content\">
      <div class=\"card z-depth-2\" style=\"padding:36px\">
        {% component 'account' %}
      </div>
    </div>
  </div>
</section>", "/home/littleflower/Workspace/~web/lembaran-caramel-2.0-beta/themes/laratify-octobercms-octaskin/pages/user.htm", "");
    }
}
