<?php

/* /home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/rilisan.htm */
class __TwigTemplate_688e32b4a085b4bd7361329bf3ba3fea28f23d2b526315f730aeb66451356ad8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["posts"] = $this->getAttribute(($context["blogPosts"] ?? null), "posts", array());
        // line 2
        echo "
<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <div class=\"card collection with-header z-depth-2\">
                <div class=\"collection-header\">
                    <p/>
                    <h2 class=\"lt-title\">Rilisan</h2>
                    <p class=\"flow-text\">Hayoo, kamu sedang menunggu rilisan dari sini, ya? ~</p>
                </div>
                <!-- do for here -->
                ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 14
            echo "                <div class=\"collection-item\" id=\"card-kabar\">
                    <div class=\"row rilisan-row\">
                        <div class=\"lt-col col m2 s12\">
                            <a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\">
                                ";
            // line 18
            if ($this->getAttribute($this->getAttribute($context["post"], "featured_images", array()), "count", array())) {
                // line 19
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute($context["post"], "featured_images", array()), 0, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 20
                    echo "                                <img class=\"responsive-img circle\" data-src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename", array()), "html", null, true);
                    echo "\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "path", array()), "html", null, true);
                    echo "\"
                                     alt=\"";
                    // line 21
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "description", array()) != null)) ? ($this->getAttribute($context["image"], "description", array())) : ($this->getAttribute($context["image"], "filename", array()))), "html", null, true);
                    echo "\"
                                     title=\"";
                    // line 22
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "title", array()) != null)) ? ($this->getAttribute($context["image"], "title", array())) : ($this->getAttribute($context["post"], "title", array()))), "html", null, true);
                    echo "\">
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 24
                echo "                                ";
            } else {
                // line 25
                echo "                                <img class=\"responsive-img circle-img\" alt=\"image\"
                                     src=\"";
                // line 26
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/pages/portfolio/mainpage/img-01.jpg");
                echo "\">
                                ";
            }
            // line 28
            echo "                            </a>
                        </div>
                        <div class=\"lt-col col m10 s12\">
                            <span class=\"right small\">";
            // line 31
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["post"], "published_at", array()), "M d, Y"), "html", null, true);
            echo "</span><br>
                            <span class=\"card-title left rilisan-list\"><a href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</span></a><br/>
                            <p class=\"left rilisan-list\">";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "summary", array()), "html", null, true);
            echo "</p>
                        </div>
                    </div>
                </div>
                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 38
            echo "                <div class=\"collection-item\" id=\"card-kabar\">
                    <h5><center class=\"align-center center\">";
            // line 39
            echo twig_escape_filter($this->env, ($context["noPostsMessage"] ?? null), "html", null, true);
            echo "</center></h5>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "                <div class=\"collection-item\" style=\"overflow: hidden;\">
                    ";
        // line 43
        if (($this->getAttribute(($context["posts"] ?? null), "lastPage", array()) > 1)) {
            // line 44
            echo "                        <a href=\"/rilisan/2\" class=\"btn-floating btn-large waves-effect waves-light right pink\"><i class=\"material-icons\">chevron_right</i></a>
                    ";
        }
        // line 46
        echo "                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/rilisan.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 46,  127 => 44,  125 => 43,  122 => 42,  113 => 39,  110 => 38,  100 => 33,  94 => 32,  90 => 31,  85 => 28,  80 => 26,  77 => 25,  74 => 24,  66 => 22,  62 => 21,  55 => 20,  50 => 19,  48 => 18,  44 => 17,  39 => 14,  34 => 13,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set posts = blogPosts.posts %}

<div class=\"center\">
    <div class=\"lt-row row\">
        <div class=\"lt-col col s12\">
            <div class=\"card collection with-header z-depth-2\">
                <div class=\"collection-header\">
                    <p/>
                    <h2 class=\"lt-title\">Rilisan</h2>
                    <p class=\"flow-text\">Hayoo, kamu sedang menunggu rilisan dari sini, ya? ~</p>
                </div>
                <!-- do for here -->
                {% for post in posts %}
                <div class=\"collection-item\" id=\"card-kabar\">
                    <div class=\"row rilisan-row\">
                        <div class=\"lt-col col m2 s12\">
                            <a href=\"{{ post.url }}\">
                                {% if post.featured_images.count %}
                                {% for image in post.featured_images|slice(0, 1) %}
                                <img class=\"responsive-img circle\" data-src=\"{{ image.filename }}\" src=\"{{ image.path }}\"
                                     alt=\"{{ image.description != null ? image.description : image.filename }}\"
                                     title=\"{{ image.title != null ? image.title : post.title }}\">
                                {% endfor %}
                                {% else %}
                                <img class=\"responsive-img circle-img\" alt=\"image\"
                                     src=\"{{ 'assets/img/pages/portfolio/mainpage/img-01.jpg'|theme }}\">
                                {% endif %}
                            </a>
                        </div>
                        <div class=\"lt-col col m10 s12\">
                            <span class=\"right small\">{{ post.published_at|date('M d, Y') }}</span><br>
                            <span class=\"card-title left rilisan-list\"><a href=\"{{ post.url }}\">{{ post.title }}</span></a><br/>
                            <p class=\"left rilisan-list\">{{ post.summary }}</p>
                        </div>
                    </div>
                </div>
                {% else %}
                <div class=\"collection-item\" id=\"card-kabar\">
                    <h5><center class=\"align-center center\">{{ noPostsMessage }}</center></h5>
                </div>
                {% endfor %}
                <div class=\"collection-item\" style=\"overflow: hidden;\">
                    {% if posts.lastPage > 1 %}
                        <a href=\"/rilisan/2\" class=\"btn-floating btn-large waves-effect waves-light right pink\"><i class=\"material-icons\">chevron_right</i></a>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>", "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/partials/pages-home/rilisan.htm", "");
    }
}
