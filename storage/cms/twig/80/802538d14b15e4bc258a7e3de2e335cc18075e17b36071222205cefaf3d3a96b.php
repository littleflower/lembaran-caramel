<?php

/* /home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/pages/lembaran.htm */
class __TwigTemplate_a585470f72b613bcac9fcb793ee5457b2f88189719977a6480d639b9853592de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($context["post"] ?? null)) {
            // line 2
            echo "
";
            // line 3
            echo $this->env->getExtension('CMS')->startBlock('seo'            );
            // line 4
            echo "
<meta name=\"description\" content=\"";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "summary", array()), "html", null, true);
            echo "\">
<meta name=\"keywords\" content=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "keywords", array()), "html", null, true);
            echo "\">
<meta name=\"author\" content=\"";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_author", array()), "html", null, true);
            echo "\">

<meta property=\"og:title\" content=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "title", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_name", array()), "html", null, true);
            echo "\"/>
<meta property=\"og:url\" content=\"";
            // line 10
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()));
            echo "\"/>
<meta property=\"og:type\" content=\"article\"/>
<meta property=\"og:description\" content=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "summary", array()), "html", null, true);
            echo "\"/>
<meta name=\"twitter:card\" content=\"summary\"/>
<meta name=\"twitter:title\" content=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "title", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_name", array()), "html", null, true);
            echo "\"/>
<meta name=\"twitter:url\" content=\"";
            // line 15
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()));
            echo "\"/>

<!-- <meta http-equiv=\"Content-Security-Policy\" content=\"script-src 'self' 'unsafe-inline' 'unsafe-eval' https://caramelfansub.disqus.com https://ajax.googleapis.com https://cdnjs.cloudflare.com http://st.chatango.com https://cdn.plyr.io;\"/> -->

";
            // line 19
            if ($this->getAttribute($this->getAttribute(($context["post"] ?? null), "featured_images", array()), "count", array())) {
                // line 20
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute(($context["post"] ?? null), "featured_images", array()), 1, 2));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 21
                    echo "<meta property=\"og:image\" content=\"";
                    echo $this->env->getExtension('System\Twig\Extension')->appFilter($this->getAttribute($context["image"], "path", array()));
                    echo "\"/>
<meta name=\"twitter:image\" content=\"";
                    // line 22
                    echo $this->env->getExtension('System\Twig\Extension')->appFilter($this->getAttribute($context["image"], "path", array()));
                    echo "\"/>
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            } else {
            }
            // line 26
            echo "
<title>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_name", array()), "html", null, true);
            echo "</title>
<link rel=\"canonical\" href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "website_url", array()), "html", null, true);
            echo "\"/>

";
            // line 3
            echo $this->env->getExtension('CMS')->endBlock(true            );
            // line 31
            echo "
<section id=\"lt-header\" class=\"lt-section lt-section-fullwidth section\">
    <div class=\"lt-container container\">
        <div class=\"lt-content lt-header-content\">
            <div class=\"lt-row row\">
                <div class=\"lt-col col s12\">
                    <div class=\"lt-parallax-header parallax-container\" style=\"height: 400px;\">
                        <div class=\"lt-parallax parallax\">
                            ";
            // line 39
            if (($this->getAttribute($this->getAttribute(($context["post"] ?? null), "featured_images", array()), "count", array()) > 1)) {
                // line 40
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute(($context["post"] ?? null), "featured_images", array()), 1, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 41
                    echo "                                    <img data-src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename", array()), "html", null, true);
                    echo "\" class=\"responsive-img\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "path", array()), "html", null, true);
                    echo "\"
                                         alt=\"";
                    // line 42
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "description", array()) != null)) ? ($this->getAttribute($context["image"], "description", array())) : ($this->getAttribute($context["image"], "filename", array()))), "html", null, true);
                    echo "\"
                                         title=\"";
                    // line 43
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "title", array()) != null)) ? ($this->getAttribute($context["image"], "title", array())) : ($this->getAttribute(($context["post"] ?? null), "title", array()))), "html", null, true);
                    echo "\"></div>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "                            ";
            } else {
                // line 46
                echo "                                <img alt=\"image\" class=\"responsive-img\" src=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/pages/1701/header-404.jpg");
                echo "\">
                            ";
            }
            // line 48
            echo "                        <div class=\"lt-parallax-content\">
                            <div class=\"valign-wrapper\" style=\"height: 400px;\">
                                <div class=\"valign\">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id=\"lt-mainpage\" class=\"lt-section section orange accent-1\">
    <div class=\"lt-container container\">
        <div class=\"lt-content lt-mainpage-content\">
            <div class=\"lt-row row\">
                <div class=\"collection card z-depth-2\">
                    <div class=\"card-image\">
                        ";
            // line 67
            if (($this->getAttribute($this->getAttribute(($context["post"] ?? null), "featured_images", array()), "count", array()) > 2)) {
                // line 68
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute(($context["post"] ?? null), "featured_images", array()), 2, 2));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 69
                    echo "                                <img data-src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename", array()), "html", null, true);
                    echo "\" class=\"responsive-img\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "path", array()), "html", null, true);
                    echo "\"
                                     alt=\"";
                    // line 70
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "description", array()) != null)) ? ($this->getAttribute($context["image"], "description", array())) : ($this->getAttribute($context["image"], "filename", array()))), "html", null, true);
                    echo "\"
                                     title=\"";
                    // line 71
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "title", array()) != null)) ? ($this->getAttribute($context["image"], "title", array())) : ($this->getAttribute(($context["post"] ?? null), "title", array()))), "html", null, true);
                    echo "\">
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 73
                echo "                        ";
            } elseif (($this->getAttribute($this->getAttribute(($context["post"] ?? null), "featured_images", array()), "count", array()) > 1)) {
                // line 74
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute(($context["post"] ?? null), "featured_images", array()), 1, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 75
                    echo "                                <img data-src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename", array()), "html", null, true);
                    echo "\" class=\"responsive-img\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "path", array()), "html", null, true);
                    echo "\"
                                     alt=\"";
                    // line 76
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "description", array()) != null)) ? ($this->getAttribute($context["image"], "description", array())) : ($this->getAttribute($context["image"], "filename", array()))), "html", null, true);
                    echo "\"
                                     title=\"";
                    // line 77
                    echo twig_escape_filter($this->env, ((($this->getAttribute($context["image"], "title", array()) != null)) ? ($this->getAttribute($context["image"], "title", array())) : ($this->getAttribute(($context["post"] ?? null), "title", array()))), "html", null, true);
                    echo "\">
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 79
                echo "                        ";
            } else {
                // line 80
                echo "                            <img alt=\"image\" class=\"responsive-img\" src=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/pages/1701/header-404.jpg");
                echo "\">
                        ";
            }
            // line 82
            echo "                    </div>
                    <div class=\"card-panel\">
                        <span class=\"right small\">";
            // line 84
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["post"] ?? null), "published_at", array()), "d M Y"), "html", null, true);
            echo "</span>
                        
                        ";
            // line 86
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["post"] ?? null), "categories", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 87
                echo "                            ";
                if (($this->getAttribute($context["category"], "name", array()) == "Garapan Selesai")) {
                    // line 88
                    echo "                                <span class=\"chip green\">Selesai</span>
                            ";
                } elseif (($this->getAttribute(                // line 89
$context["category"], "name", array()) == "Garapan Lancar")) {
                    // line 90
                    echo "                                <span class=\"chip blue\">Lancar</span>
                            ";
                } elseif (($this->getAttribute(                // line 91
$context["category"], "name", array()) == "Garapan Terhambat")) {
                    // line 92
                    echo "                                <span class=\"chip yellow darken-3\">Lancar?</span>
                            ";
                } elseif (($this->getAttribute(                // line 93
$context["category"], "name", array()) == "Garapan Batal")) {
                    // line 94
                    echo "                                <span class=\"chip red\">Batal</span>
                            ";
                } elseif (($this->getAttribute(                // line 95
$context["category"], "name", array()) == "Sedang Tayang")) {
                    // line 96
                    echo "                                <span class=\"chip purple\">Tayang</span>
                            ";
                } elseif (call_user_func_array($this->env->getFunction('str_contains')->getCallable(), array("contains", $this->getAttribute(                // line 97
$context["category"], "slug", array()), "join-"))) {
                    // line 98
                    echo "                                <span class=\"chip pink\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                    echo "</span>
                            ";
                }
                // line 100
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 101
            echo "                        
                        <br>
                        
                        <span class=\"card-title\"><h5>";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "title", array()), "html", null, true);
            echo "</h5></span>                        
                        <p>";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "summary", array()), "html", null, true);
            echo "</p>
        
                        <div class=\"lt-row row\">
                            <div class=\"lt-col col s12\">
                                ";
            // line 109
            echo $this->getAttribute(($context["post"] ?? null), "content_html", array());
            echo "
                            </div>
                        </div>
                    </div>
                    <div id=\"disqus_thread\" class=\"collection-item\"></div>
                </div>
            </div>
        </div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

var disqus_config = function () {
this.page.url = \"";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "url", array()), "html", null, true);
            echo "\";  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = \"";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "id", array()), "html", null, true);
            echo "\"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://caramelfansub.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href=\"https://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>
                                
    </div>
</section>

";
        } else {
            // line 140
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('CMS')->partialFunction("not_found"            , $context['__cms_partial_params']            );
            unset($context['__cms_partial_params']);
        }
    }

    public function getTemplateName()
    {
        return "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/pages/lembaran.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 140,  325 => 125,  321 => 124,  303 => 109,  296 => 105,  292 => 104,  287 => 101,  281 => 100,  275 => 98,  273 => 97,  270 => 96,  268 => 95,  265 => 94,  263 => 93,  260 => 92,  258 => 91,  255 => 90,  253 => 89,  250 => 88,  247 => 87,  243 => 86,  238 => 84,  234 => 82,  228 => 80,  225 => 79,  217 => 77,  213 => 76,  206 => 75,  201 => 74,  198 => 73,  190 => 71,  186 => 70,  179 => 69,  174 => 68,  172 => 67,  151 => 48,  145 => 46,  142 => 45,  134 => 43,  130 => 42,  123 => 41,  118 => 40,  116 => 39,  106 => 31,  104 => 3,  99 => 28,  95 => 27,  92 => 26,  82 => 22,  77 => 21,  73 => 20,  71 => 19,  64 => 15,  58 => 14,  53 => 12,  48 => 10,  42 => 9,  37 => 7,  33 => 6,  29 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if post %}

{% put seo %}

<meta name=\"description\" content=\"{{ post.summary }}\">
<meta name=\"keywords\" content=\"{{ this.theme.keywords }}\">
<meta name=\"author\" content=\"{{ this.theme.website_author }}\">

<meta property=\"og:title\" content=\"{{ post.title }} - {{ this.theme.website_name }}\"/>
<meta property=\"og:url\" content=\"{{ this.page.id | page }}\"/>
<meta property=\"og:type\" content=\"article\"/>
<meta property=\"og:description\" content=\"{{ post.summary }}\"/>
<meta name=\"twitter:card\" content=\"summary\"/>
<meta name=\"twitter:title\" content=\"{{ post.title }} - {{ this.theme.website_name }}\"/>
<meta name=\"twitter:url\" content=\"{{ this.page.id | page }}\"/>

<!-- <meta http-equiv=\"Content-Security-Policy\" content=\"script-src 'self' 'unsafe-inline' 'unsafe-eval' https://caramelfansub.disqus.com https://ajax.googleapis.com https://cdnjs.cloudflare.com http://st.chatango.com https://cdn.plyr.io;\"/> -->

{% if post.featured_images.count %}
{% for image in post.featured_images|slice(1, 2) %}
<meta property=\"og:image\" content=\"{{ image.path|app }}\"/>
<meta name=\"twitter:image\" content=\"{{ image.path|app }}\"/>
{% endfor %}
{% else %}
{% endif %}

<title>{{ this.theme.website_name }}</title>
<link rel=\"canonical\" href=\"{{ this.theme.website_url }}\"/>

{% endput %}

<section id=\"lt-header\" class=\"lt-section lt-section-fullwidth section\">
    <div class=\"lt-container container\">
        <div class=\"lt-content lt-header-content\">
            <div class=\"lt-row row\">
                <div class=\"lt-col col s12\">
                    <div class=\"lt-parallax-header parallax-container\" style=\"height: 400px;\">
                        <div class=\"lt-parallax parallax\">
                            {% if post.featured_images.count > 1 %}
                                {% for image in post.featured_images|slice(1, 1) %}
                                    <img data-src=\"{{ image.filename }}\" class=\"responsive-img\" src=\"{{ image.path }}\"
                                         alt=\"{{ image.description != null ? image.description : image.filename }}\"
                                         title=\"{{ image.title != null ? image.title : post.title }}\"></div>
                                {% endfor %}
                            {% else %}
                                <img alt=\"image\" class=\"responsive-img\" src=\"{{ 'assets/img/pages/1701/header-404.jpg'|theme }}\">
                            {% endif %}
                        <div class=\"lt-parallax-content\">
                            <div class=\"valign-wrapper\" style=\"height: 400px;\">
                                <div class=\"valign\">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id=\"lt-mainpage\" class=\"lt-section section orange accent-1\">
    <div class=\"lt-container container\">
        <div class=\"lt-content lt-mainpage-content\">
            <div class=\"lt-row row\">
                <div class=\"collection card z-depth-2\">
                    <div class=\"card-image\">
                        {% if post.featured_images.count > 2 %}
                            {% for image in post.featured_images|slice(2, 2) %}
                                <img data-src=\"{{ image.filename }}\" class=\"responsive-img\" src=\"{{ image.path }}\"
                                     alt=\"{{ image.description != null ? image.description : image.filename }}\"
                                     title=\"{{ image.title != null ? image.title : post.title }}\">
                            {% endfor %}
                        {% elseif post.featured_images.count > 1 %}
                            {% for image in post.featured_images|slice(1, 1) %}
                                <img data-src=\"{{ image.filename }}\" class=\"responsive-img\" src=\"{{ image.path }}\"
                                     alt=\"{{ image.description != null ? image.description : image.filename }}\"
                                     title=\"{{ image.title != null ? image.title : post.title }}\">
                            {% endfor %}
                        {% else %}
                            <img alt=\"image\" class=\"responsive-img\" src=\"{{ 'assets/img/pages/1701/header-404.jpg'|theme }}\">
                        {% endif %}
                    </div>
                    <div class=\"card-panel\">
                        <span class=\"right small\">{{ post.published_at|date('d M Y') }}</span>
                        
                        {% for category in post.categories %}
                            {% if category.name == \"Garapan Selesai\" %}
                                <span class=\"chip green\">Selesai</span>
                            {% elseif category.name == \"Garapan Lancar\" %}
                                <span class=\"chip blue\">Lancar</span>
                            {% elseif category.name == \"Garapan Terhambat\" %}
                                <span class=\"chip yellow darken-3\">Lancar?</span>
                            {% elseif category.name == \"Garapan Batal\" %}
                                <span class=\"chip red\">Batal</span>
                            {% elseif category.name == \"Sedang Tayang\" %}
                                <span class=\"chip purple\">Tayang</span>
                            {% elseif str_contains(category.slug, 'join-') %}
                                <span class=\"chip pink\">{{ category.name }}</span>
                            {% endif %}
                        {% endfor %}
                        
                        <br>
                        
                        <span class=\"card-title\"><h5>{{ post.title }}</h5></span>                        
                        <p>{{ post.summary }}</p>
        
                        <div class=\"lt-row row\">
                            <div class=\"lt-col col s12\">
                                {{ post.content_html|raw }}
                            </div>
                        </div>
                    </div>
                    <div id=\"disqus_thread\" class=\"collection-item\"></div>
                </div>
            </div>
        </div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

var disqus_config = function () {
this.page.url = \"{{ post.url }}\";  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = \"{{ post.id }}\"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://caramelfansub.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href=\"https://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>
                                
    </div>
</section>

{% else %}
{% partial \"not_found\" %}
{% endif %}", "/home/littleflower/Workspace/~web/backup-caramel-4-mei/themes/laratify-octobercms-octaskin/pages/lembaran.htm", "");
    }
}
